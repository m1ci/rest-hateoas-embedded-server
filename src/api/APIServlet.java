package api;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.stream.Collectors;
import com.google.gson.Gson;
import db.*;

@WebServlet("/*")
public class APIServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String[] path = request.getPathInfo().split("/");
	    if(path.length>2 && path[1].equalsIgnoreCase("order")) {
	            DB db = DB.getInstance();
	            response.setStatus(200);
	            response.setHeader("Content-Type" , "application/json");
	            Gson gson = new Gson();
	            Order order = db.getObject(Integer.valueOf(path[2]));
	            Links links = new Links(order, new Link[]{new Link("http://127.0.0.1:7001/tutorial3/order/", "add")});
	            response.getWriter().println(gson.toJson(links));
	    } else {
	            response.setStatus(400);
	    }
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String[] path = request.getPathInfo().split("/");
	    if(path.length==2 && path[1].equalsIgnoreCase("order")) {
	            String data = request.getReader().lines().collect(Collectors.joining());
	            Gson gson = new Gson();
	            Order c = gson.fromJson(data, Order.class);
	            c.setId(1);
	            DB db = DB.getInstance();
	            db.addObject(c);
	            response.setStatus(201);
	    } else {
	            response.setStatus(400);
	    }
	}

}
