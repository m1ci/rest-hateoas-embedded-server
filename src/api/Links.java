package api;

import db.*;
 
public class Links {

    private Order order;
    private Link[] links;
 
    public Links(Order order, Link[] links) {
        this.order = order;
        this.links = links;
    }
 
    public Order getOrder() {
        return order;
    }
 
    public void setOrder(Order order) {
        this.order = order;
    }

    public Link[] getLinks() {
        return links;
    }
 
    public void setLinks(Link[] links) {
        this.links = links;
    }

}