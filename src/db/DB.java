package db;
import java.util.Hashtable;
 
public class DB {
    private static DB instance = null;
    private Hashtable<Integer, Order> ht = new Hashtable();
 
    public static DB getInstance() {
        if (instance == null)
            instance = new DB();
        return instance;
    }
    public void addObject(Order t){
        ht.put(t.getId(), t);
    }
    public Order getObject(int id) {
        return ht.get(id);
    }
}